<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $fillable = [
        'name', 'imagename','phone','email'
    ];

    public function students()
    {
        return $this->hasMany(Student::class,'teacher_id','id');
    }
    
}
