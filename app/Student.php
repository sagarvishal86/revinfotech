<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'name', 'imagename','phone','email'
    ];
    
    public function teacher()
    {
        return $this->belongsTo(Teacher::class);
    }
}
