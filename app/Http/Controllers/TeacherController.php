<?php

namespace App\Http\Controllers;

use App\Teacher;
use Illuminate\Http\Request;
use Image;
use Validator;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $teachers = Teacher::latest()->paginate(5);
  
        return view('teachers.index',compact('teachers'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('teachers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'imagename' => 'required',
        ]);

        if($request->hasfile('imagename'))
        {
           
            $imagename = $request->file('imagename');
            $filename = time() . '.' . $imagename->getClientOriginalExtension();
            Image::make($imagename)->resize(300, 300)->save( public_path('/uploads/profile_image/' . $filename) );
            if(!$filename)
            {           
                return redirect()->route('teacher.create')->with('error','Please select valid image!');
            }
        }
        $teacher = new Teacher;
        $teacher->name = $request->post('name');
        $teacher->phone = $request->post('phone');
        $teacher->email = $request->post('email');
        $teacher->Description = $request->post('Description');
        $teacher->imagename = isset($filename)?$filename:'default.png';
        // dd($teacher);
        $teacher->save();
        // Teacher::create($request->all());
   
        return redirect()->route('teachers.index')
                        ->with('success','Teacher added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function show(Teacher $teacher)
    {
        $students = Teacher::find($teacher->id)->students;
        return view('teachers.show',compact('teacher','students'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function edit(Teacher $teacher)
    {
        return view('teachers.edit',compact('teacher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Teacher $teacher)
    {
        $request->validate([
            'name' => 'required',
        ]);
       
        if($request->hasfile('imagename'))
        {
           
            $imagename = $request->file('imagename');
            $filename = time() . '.' . $imagename->getClientOriginalExtension();
            Image::make($imagename)->resize(300, 300)->save( public_path('/uploads/profile_image/' . $filename) );
            if(!$filename)
            {           
                return redirect()->route('teachers.edit')->with('error','Please select valid image!');
            }
        } else
        {
            $filename=$request->post('prev_image');
        }

        $teacher=Teacher::find($teacher->id);
        $teacher->name = $request->post('name');
        $teacher->phone = $request->post('phone');
        $teacher->email = $request->post('email');
        $teacher->Description = $request->post('Description');
        $teacher->imagename = isset($filename)?$filename:'default.png';
        // dd($teacher);
        $teacher->save();

        // $teacher->update($request->all());
  
        return redirect()->route('teachers.index')
                        ->with('success','Teacher updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function destroy(Teacher $teacher)
    {
        $teacher->students()->delete();
        $teacher->delete();
  
        return redirect()->route('teachers.index')
                        ->with('success','Teacher deleted successfully');
    }
}
