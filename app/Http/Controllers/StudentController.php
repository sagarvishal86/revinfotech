<?php

namespace App\Http\Controllers;

use App\Student;
use App\Teacher;
use Illuminate\Http\Request;
use Image;
use Validator;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teachers = Teacher::pluck('name', 'id');
        $students = Student::latest()->paginate(5);
        // dd($students);
        return view('students.index',compact('students','teachers'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teachers = Teacher::pluck('name', 'id');
        return view('students.create',compact('teachers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:50',
            'imagename' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'teacher_id' => 'required|numeric',
            'phone' => 'required|digits:10',
            'email' => 'required|email',
        ],
        [
            'imagename.required' => 'Profile picture should not be empty',
            'imagename.image' => 'Profile picture must be a file of type: jpeg, png, jpg.',
        ]
        );

        if($request->hasfile('imagename'))
        {
           
            $imagename = $request->file('imagename');
            $filename = time() . '.' . $imagename->getClientOriginalExtension();
            Image::make($imagename)->resize(300, 300)->save( public_path('/uploads/profile_image/' . $filename) );
            if(!$filename)
            {           
                return redirect()->route('student.create')->with('error','Please select valid image!');
            }
        }
        $student = new Student;
        $student->name = $request->post('name');
        $student->teacher_id = $request->post('teacher_id');
        $student->phone = $request->post('phone');
        $student->email = $request->post('email');
        $student->imagename = isset($filename)?$filename:'default.png';
        // dd($student);
        $student->save();
   
        return redirect()->route('students.index')
                        ->with('success','Student added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        return view('students.show',compact('student'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        $teachers = Teacher::pluck('name', 'id');
        $selectedID = $student->teacher_id;
        return view('students.edit',compact('student','teachers','selectedID'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        // dd($student->id);
        $request->validate([
            'name' => 'required|max:50',
            'imagename' => 'image|mimes:jpeg,png,jpg|max:2048',
            'teacher_id' => 'required|numeric',
            'phone' => 'required|digits:10',
            'email' => 'required|email',
        ]);
            
        if($request->hasfile('imagename'))
        {
           
            $imagename = $request->file('imagename');
            $filename = time() . '.' . $imagename->getClientOriginalExtension();
            Image::make($imagename)->resize(300, 300)->save( public_path('/uploads/profile_image/' . $filename) );
            if(!$filename)
            {           
                return redirect()->route('students.edit')->with('error','Please select valid image!');
            }
        }else
        {
            $filename=$request->post('prev_image');
        }

        $stu = Student::find($student->id);
        $stu->name = $request->post('name');
        $stu->teacher_id = $request->post('teacher_id');
        $stu->phone = $request->post('phone');
        $stu->email = $request->post('email');
        $stu->imagename = isset($filename)?$filename:'default.png';
        if($stu->save()){
            return redirect()->route('students.index')
                        ->with('success','Student updated successfully');
        } else {
            return redirect()->route('students.index')
                        ->with('error','Student is not updated');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        $student->delete();
  
        return redirect()->route('students.index')
                        ->with('success','Student deleted successfully');
    }
}
