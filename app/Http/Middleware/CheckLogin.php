<?php

namespace App\Http\Middleware;

use Closure;

class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd($request->user());
        if($request->user()){
            return $next($request);
            
        }
        else
        {
            return redirect()->route('login')->with('error',"You don't have permission to this page!");
        }
        
        return $next($request);
    }
}
