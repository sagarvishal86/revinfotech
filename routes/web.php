<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('teachers','TeacherController')->middleware('checkLogin');

Route::resource('students','StudentController')->middleware('checkLogin');

Route::get('/addStudent',['as' => 'students.create','uses' =>'StudentController@create']);


Route::middleware(['checkLogin'])->group(function() {

    Route::resource('teachers', 'TeacherController', [
        'only' => [
            'index', 'store', 'update'
        ]
    ]);

    Route::resource('students', 'StudentController', [
        'only' => [
            'index', 'store', 'update'
        ]
    ]);

});

Route::get('logout', function ()
{
    auth()->logout();
    Session()->flush();

    return Redirect::to('/login');
})->name('logout');