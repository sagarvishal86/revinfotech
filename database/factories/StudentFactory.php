<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Student;
use Faker\Generator as Faker;

$factory->define(Student::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'teacher_id' => factory('App\Teacher')->create()->id,
        'imagename' => 'default.jpg',
        'phone' => $faker->numerify('##########'),
        'email' => $faker->email,
    ];
});
