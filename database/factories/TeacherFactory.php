<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Teacher;
use Faker\Generator as Faker;

$factory->define(Teacher::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'imagename' => 'default.jpg',
        'phone' => $faker->numerify('##########'),
        'email' => $faker->email,
        'description' => $faker->sentence(50),
    ];
});
