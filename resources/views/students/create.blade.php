@extends('layouts.app')
  
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Add New Student</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('students.index') }}"> Back</a>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form enctype="multipart/form-data" action="{{ route('students.store') }}" method="POST">
    @csrf
  
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name <span>*</span></strong>
                <input type="text" name="name" required class="form-control" placeholder="Name">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Phone <span>*</span></strong>
                <input type="text" name="phone" required class="form-control" placeholder="Phone">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Email <span>*</span></strong>
                <input type="email" name="email" required class="form-control" placeholder="Email">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Teacher <span>*</span></strong>
                <select class="form-control" name="teacher_id">
                    <option>Select Teacher</option>
                    @foreach ($teachers as $key => $value)
                        <option value="{{ $key }}"> 
                            {{ $value }} 
                        </option>
                    @endforeach    
                </select>
            </div>
        </div>
        
        <div class="form-group col-md-6">
                <label>Profile Picture <span>*</span></label>             
                
                    <input type="file" name="imagename" class="form-control">
                    <input type="hidden" class="token_value" name="_token" value="{{csrf_token()}}">                  
                
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
   
</form>
</div>

</div>
</div>
</div>

@endsection