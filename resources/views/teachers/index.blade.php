@extends('layouts.app')
 
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Teachers</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('teachers.create') }}"> Add New Teacher</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>Name</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Image</th>
            <th>Description</th>

            <th width="280px">Action</th>
        </tr>
        @foreach ($teachers as $teacher)
        <tr>
            <td><a href="{{ route('teachers.show',$teacher->id) }}">{{ $teacher->name }}</a></td>
            <td>{{ $teacher->phone }}</td>
            <td>{{ $teacher->email }}</td>
            
            <td><img id="profile_pic" src="{{ URL::asset('/uploads/profile_image/'.$teacher->imagename) }}" width=50 height=50/></td>
            <td title="{{ $teacher->Description }}">{{ Str::limit($teacher->Description,50) }}</td>
            <td>
                <form action="{{ route('teachers.destroy',$teacher->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('teachers.show',$teacher->id) }}">Show</a>
    
                    <a class="btn btn-primary" href="{{ route('teachers.edit',$teacher->id) }}">Edit</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" onclick="return confirm('Are you sure, you want to delete this teacher because all the related students will also be deleted?')" class="btn btn-danger">Delete</button>
                </form>
            </td>
            
        </tr>
        @endforeach
    </table>
  
    {!! $teachers->links() !!}
      </div>
      </div>
      </div>
      </div>
@endsection